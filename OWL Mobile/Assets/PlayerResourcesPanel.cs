using UnityEngine;
using TMPro;

public class PlayerResourcesPanel : MonoBehaviour
{
    public enum ResourcesType
    {
        PLAYER, LEVEL
    }
    [SerializeField]
    private ResourcesType type;
    [SerializeField]
    private TMP_Text iron;
    [SerializeField]
    private TMP_Text gold;
    [SerializeField]
    private TMP_Text platinum;
    [SerializeField]
    private string postfix = "T";


    // Cached values
    private SO_PlayerResources.Resources resources;

    private void Start()
    {
        resources = type == ResourcesType.PLAYER ? SO_PlayerResources.Instance.resources : SO_PlayerResources.Instance.levelResources;
        resources.onChange.AddListener(UpdateResources);
        UpdateResources();
    }
    private void OnDisable()
    {
        resources.onChange.RemoveListener(UpdateResources);   
    }
    private void UpdateResources()
    {
        iron.text = resources.iron.ToString("N1") + postfix;
        gold.text = resources.gold.ToString("N1") + postfix;
        platinum.text = resources.platinum.ToString("N1") + postfix;
    }
}
