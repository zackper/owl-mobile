using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointerTag : MonoBehaviour
{
    public Transform target;

    public float offset =  200f;

    private new Camera camera;
    private RectTransform rect;

    private Vector3 screenCenter;


    [SerializeField]
    private float smoothTime = 0.1f;
    private Vector3 smoothVelocity = Vector3.zero;


    private void Start()
    {
        camera = Camera.main;
        rect = GetComponent<RectTransform>();

        screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0f);
    }

    private void Update()
    {
        Vector3 position = camera.WorldToScreenPoint(target.position);
        position.z = 0;

        Vector3 final = Vector3.zero;
        float slope = (screenCenter.x - position.x) / (screenCenter.y - position.y);
        if (position.y < screenCenter.y)
        {
            final.x = position.x + Mathf.Sin(slope) * offset;
            final.y = position.y + Mathf.Cos(slope) * offset;
        }
        else
        {
            final.x = position.x - Mathf.Sin(slope) * offset;
            final.y = position.y - Mathf.Cos(slope) * offset;
        }
        float angle = Mathf.Atan(slope);

        final.x = Mathf.Clamp(final.x, 100f, Screen.width - 100f);
        final.y = Mathf.Clamp(final.y, 100f, Screen.height - 100f);
        rect.transform.position = final;
        //rect.transform.position = Vector3.SmoothDamp(
        //    rect.transform.position,
        //    final,
        //    ref smoothVelocity,
        //    smoothTime
        //);
    }
}
