using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

// Level Director was already taken :')
public class LevelHandler : MonoBehaviour
{
    public string levelName = "Easy_1";
    private SO_Level level = null;

    private Minimap minimap;
    private GameObject player;

    private int currentOreCount = 0;
    private int currentSpacetimeTearCount = 0;

    // Start is called before the first frame update
    void Awake()
    {
        // Find references
        minimap = FindObjectOfType<Minimap>();
        player = GameObject.FindGameObjectWithTag("Spaceship");
        minimap.AddPin(player.transform, Minimap.PinType.PLAYER);

        // Start Level Routine
        StartLevel();

        InputManager.Instance.Subscribe("escape", () =>
        {

            EndLevel();
        });
    }

    public void StartLevel()
    {
        // Get level references
        level = Resources.Load<SO_Level>(Path.Combine("SO", "Levels", levelName));
        Debug.Assert(level, "Can not find level: " + levelName);

        // Spawn Ores
        SpawnResourceMeteors();
        InvokeRepeating("TrySpawnSpacetimeTear", 0, 1);

        // Set level specific G
        Attractor.G = level.G;

        // Reset player resources
        SO_PlayerResources.Instance.ResetLevelResources();
    }
    public void EndLevel()
    {
        SO_PlayerResources.Instance.CommitLevelResources();
        SceneManager.LoadScene("Menu");
    }
    private void OnApplicationQuit()
    {
        EndLevel();
    }

    #region Spawn Functions
    private void SpawnResourceMeteors()
    {
        for(int i = 0; i < level.ores.Count; i++)
        {
            for (int j = 0; j < level.amount[i]; j++) {
                // Spawn ore and initialize from SO
                GameObject orePrefab = level.orePrefabs[Random.Range(0, level.orePrefabs.Count)];
                GameObject oreInstance = Instantiate(orePrefab, this.transform);
                oreInstance.transform.position = GetRandomPosition();
                    
                ResourceMeteor ore = oreInstance.GetComponent<ResourceMeteor>();
                ore.InitializeOre(level.ores[i]);

                // Increase ore count
                ++currentOreCount;
                // Add to minimap
                minimap.AddPin(ore.transform, Minimap.PinType.RESOURCE_METEOR);
                ore.onDeath.AddListener(delegate
                {
                    minimap.RemovePin(ore.transform);
                    --currentOreCount;
                });
            }
        }
    }
    private void TrySpawnSpacetimeTear()
    {
        if (currentSpacetimeTearCount >= level.maxSpacetimeTears)
            return;

        if(Random.Range(0f, 1f) < level.spacetimeTearChancePerSecond)
        {
            GameObject tearInstance = Instantiate(level.spacetimeTearPrefab, this.transform);
            SpacetimeTear tear = tearInstance.GetComponent<SpacetimeTear>();
            tear.transform.position = GetRandomPosition();

            // Increase tear count
            ++currentSpacetimeTearCount;
            // Add to minimap
            minimap.AddPin(tearInstance.transform, Minimap.PinType.SPACETIME_TEAR);
            tear.onDeath.AddListener(delegate
            {
                minimap.RemovePin(tear.transform);
                --currentSpacetimeTearCount;
            });
        }
    }

    private Vector3 GetRandomPosition()
    {
        Debug.Assert(level, "Level should not be null: " + levelName);
        float radius = level.maxDistance;
        float safeRadius = level.safeDistance;

        Vector3 position = Vector3.zero;
        while (true)
        {
            float x = Random.Range(-radius, radius);
            float y = Random.Range(-radius, radius);
            position = new Vector3(x, 0f, y);
            if (Vector3.Distance(position, Vector3.zero) < safeRadius)
                continue;
            else
                break;
        }

        return position;
    }
    #endregion
}
