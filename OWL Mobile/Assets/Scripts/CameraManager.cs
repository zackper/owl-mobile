using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    public float defaultCameraDistance = 200f;
    [SerializeField]
    public float defaultFocusDistance = 120f;

    [SerializeField]
    private Transform       cameraTarget;
    private TopdownCamera   topdownCamera;

    private GameObject      spaceship;

    /**
     * Centers camera target between camera and object and zooms accordingly
     */
    public void FocusObject(GameObject obj)
    {
        cameraTarget.SetParent(transform);
        cameraTarget.position = Vector3.Lerp(spaceship.transform.position, obj.transform.position, 0.5f);

        float slope = (obj.transform.position.x - spaceship.transform.position.x) / (obj.transform.position.z - spaceship.transform.position.z);
        float angle = Mathf.Atan(slope);
        topdownCamera.SetPivotEulerAngles(new Vector3(-40f, angle * Mathf.Rad2Deg + 130f, 0f));
        topdownCamera.SetCameraDistance(defaultFocusDistance);
    }
    public void LockToSpaceship()
    {
        cameraTarget.SetParent(spaceship.transform);
        cameraTarget.localPosition = Vector3.zero;

        topdownCamera.SetPivotEulerAngles(new Vector3(-25, 0f, 0f));
        topdownCamera.SetCameraDistance(defaultCameraDistance);
    }

    #region Events
    private void RegisterEvents()
    {
        QtEventManager.Instance.onEventStart += OnEventStart;
        QtEventManager.Instance.onEventEnd += OnEventEnd;
    }
    private void UnregisterEvents()
    {
        QtEventManager.Instance.onEventStart -= OnEventStart;
        QtEventManager.Instance.onEventEnd -= OnEventEnd;
    }
    private void OnEventStart(QtEvent e){
        FocusObject(e.gameObject);
    }
    private void OnEventEnd(QtEvent e)
    {
        LockToSpaceship();
    }
    #endregion

    #region Singleton Pattern
    public static CameraManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);

        topdownCamera = Camera.main.GetComponent<TopdownCamera>();
        spaceship = GameObject.FindGameObjectWithTag("Spaceship");
        LockToSpaceship();
    }
    private void Start()
    {
        RegisterEvents();
    }
    #endregion
}
