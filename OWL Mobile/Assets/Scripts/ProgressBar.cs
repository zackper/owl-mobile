using UnityEngine;
using System.IO;
using UnityEngine.UI;
using TMPro;

public class ProgressBar : MonoBehaviour
{
    [SerializeField]
    private Image       progressBar;
    [SerializeField]
    private TMP_Text    percentageText;
    [SerializeField]
    private string      postfix;

    public float GetProgressFill()
    {
        return progressBar.fillAmount;
    }
    public void SetProgressFill(float percentage)
    {
        if (percentage < 0f)
            return;

        Debug.Assert(percentage <= 1f, "Percentage should be between 0 and 1");
        progressBar.fillAmount = percentage;

        percentageText.text = ""; // Bug with greek characaters somewhere. Remove later.
        percentageText.text = (percentage * 100f).ToString("0") + postfix;
    }
}
