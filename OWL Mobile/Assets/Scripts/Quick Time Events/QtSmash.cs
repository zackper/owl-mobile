using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QtSmash : QtEvent
{
    public int currentRepeat = 0;

    protected new void Start()
    {
        base.Start();
        onStart.AddListener(delegate
        {
            shouldUpdate = true;
        });
    }

    protected override void EventUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ++currentRepeat;
            if (currentRepeat == repeats)
                onSuccess.Invoke();

            onPossitiveProgress.Invoke();
        }
    }
}
