using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class QtEventManager : MonoBehaviour
{
    public event Action<QtEvent> onEventStart = delegate { };
    public event Action<QtEvent> onEventEnd = delegate { };

    public bool isBusy = false;

    public void StartEvent(QtEvent e)
    {
        isBusy = true;
        onEventStart.Invoke(e);
    }
    public bool TryStartEvent(QtEvent e)
    {
        if (isBusy)
            return false;

        //Else
        isBusy = true;
        onEventStart.Invoke(e);
        return true;
    }
    public void EndEvent(QtEvent e)
    {
        isBusy = false;
        onEventEnd.Invoke(e);
    }

    #region Singleton Pattern
    public static QtEventManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);
    }
    #endregion
}
