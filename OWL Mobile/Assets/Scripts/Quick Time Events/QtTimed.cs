using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QtTimed : QtEvent
{
    [SerializeField]
    private GameObject qtTimerPrefab;
    private QtTimer qtTimer;

    public float    targetAmount = 0.3f;
    public float    speed = 1f;
    private int     currentRepeat = 0;

    private new void Start()
    {
        base.Start();
        onStart.AddListener(delegate
        {
            StartCoroutine(EventStartRoutine());
        });
        onEnd.AddListener(delegate
        {
            shouldUpdate = false;
            Destroy(qtTimer.gameObject);
            Destroy(this.gameObject);
        });
    }
    protected override void EventUpdate()
    {
        // Increment clock
        qtTimer.timer = qtTimer.timer - Time.deltaTime * speed;

        // Check if lost
        if (qtTimer.timer <= 0f)
        {
            onFailure.Invoke();
            return;
        }

        // Check input
        if (Input.GetMouseButtonDown(0))
        {
            if (qtTimer.timer > 0f && qtTimer.timer < qtTimer.target)
            {
                currentRepeat++;
                qtTimer.timer = 1f;
                qtTimer.target *= 0.9f;
                speed *= 1.2f;

                if (currentRepeat == repeats)
                    onSuccess.Invoke();
            }
            else
                onFailure.Invoke();
        }
    }

    private IEnumerator EventStartRoutine()
    {
        CameraManager.Instance.FocusObject(this.gameObject);
        yield return new WaitForSeconds(0.5f);
    
        // Start event
        GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
        qtTimer = Instantiate(qtTimerPrefab, canvas.transform).GetComponent<QtTimer>();
        qtTimer.target = targetAmount;
        shouldUpdate = true;

        yield return null;
    }
}
