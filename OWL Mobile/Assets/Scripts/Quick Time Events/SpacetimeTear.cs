using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.IO;

public class SpacetimeTear : MonoBehaviour
{
    public UnityEvent   onDeath = new UnityEvent();

    private QtEvent     qtEvent;
    private SO_Portal   portal;

    private void Awake()
    {
        portal = Resources.Load<SO_Portal>(Path.Combine("SO", "Portals", "Portal"));

        qtEvent = GetComponent<QtEvent>();
        qtEvent.onSuccess.AddListener(delegate
        {
            Destroy(this.gameObject);
        });

        StartCoroutine(DestabilizePortal(1f));
    }

    private IEnumerator DestabilizePortal(float interval)
    {
        while (true)
        {
            yield return new WaitForSeconds(interval);
            portal.stability -= portal.tearDestabilizePerSecond;
        }
    }

    private void OnDestroy()
    {
        onDeath.Invoke();
    }
}
