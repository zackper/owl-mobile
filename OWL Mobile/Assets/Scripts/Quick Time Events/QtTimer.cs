using UnityEngine;
using UnityEngine.UI;

public class QtTimer : MonoBehaviour
{
    public Image timerImage;
    public Image targetImage;

    public float timer
    {
        get
        {
            return timerImage.fillAmount;
        }
        set
        {
            timerImage.fillAmount = value;
        }
    }
    public float target
    {
        get
        {
            return targetImage.fillAmount;
        }
        set
        {
            targetImage.fillAmount = value;
        }
    }
}
