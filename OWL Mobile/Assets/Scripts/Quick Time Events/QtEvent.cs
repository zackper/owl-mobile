using System;
using UnityEngine;
using UnityEngine.Events;

public abstract class QtEvent : MonoBehaviour
{
    [SerializeField]
    protected int repeats = 1;
    protected bool hasStarted = false;
    protected bool shouldUpdate = false;
    protected float timeStarted;

    // Common QT events to allow animations and effects
    public UnityEvent onStart = new UnityEvent();
    public UnityEvent onEnd = new UnityEvent();
    public UnityEvent onSuccess = new UnityEvent();
    public UnityEvent onFailure = new UnityEvent();
    public UnityEvent onNegativeProgress = new UnityEvent();
    public UnityEvent onPossitiveProgress = new UnityEvent();

    protected void Start()
    {
        onStart.AddListener(delegate
        {
            timeStarted = Time.time;
        });
        onEnd.AddListener(delegate
        {
            QtEventManager.Instance.EndEvent(this);
        });

        onSuccess.AddListener(delegate
        {
            onEnd.Invoke();
        });
        onFailure.AddListener(delegate
        {
            onEnd.Invoke();
        });
    }
    protected void Update()
    {
        if (shouldUpdate)
            EventUpdate();
    }
    protected virtual void EventUpdate()
    {
        Debug.Assert(false, "Control flow should never reach abstract's virtual");
    }

    private void OnMouseDown()
    {
        if (hasStarted || QtEventManager.Instance.TryStartEvent(this) == false) // Allow click only once
            return;

        hasStarted = true;
        onStart.Invoke(); 
    }
    public int GetRepeats()
    {
        return repeats;
    }
    public void SetRepeats(int repeats)
    {
        this.repeats = repeats;
    }
}
