using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(QtEvent))]
public class ResourceMeteor : MonoBehaviour
{
    public SO_Ore ore;
    public SO_PlayerResources playerResources;

    [SerializeField]
    private new MeshRenderer    renderer;
    private QtEvent             qtEvent;

    private void Start()
    {
        playerResources = SO_PlayerResources.Instance;

        qtEvent = GetComponent<QtEvent>();
        qtEvent.onStart.AddListener(GatherStart);
        qtEvent.onPossitiveProgress.AddListener(GatherProgress);
        qtEvent.onSuccess.AddListener(GatherEnd);
    }
    public void InitializeOre(SO_Ore ore)
    {
        this.ore = ore;
        transform.localScale = Vector3.one * ore.scale;

        qtEvent = GetComponent<QtEvent>();
        qtEvent.SetRepeats(ore.repeats);

        // Set graphics and colors
        Material material = renderer.material;
        material.SetColor("RockColor", ore.rockColor);
        material.SetColor("MetalColor", ore.metalColor);
    }

    private void GatherStart()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }
    private void GatherProgress()
    {
        playerResources.AddLevelResources(ore.type, ore.amount / ore.repeats);

        // Animation Coroutine
        float amount = 1f / (float)qtEvent.GetRepeats();
        StartCoroutine(GatherProgressRoutine(5, amount/5));
    }
    private IEnumerator GatherProgressRoutine(int frameCount, float step)
    {
        for (int i = 0; i < frameCount; i++)
        {
            Material mat = renderer.material;
            float dissolvedAmount = mat.GetFloat("DisolveAmount");
            mat.SetFloat("DisolveAmount", dissolvedAmount + step);
            //yield return null;
            yield return new WaitForSeconds(0.05f);
        }
    }
    private void GatherEnd()
    {
        Destroy(gameObject);
    }

    public UnityEvent onDeath = new UnityEvent();
    private void OnDestroy()
    {
        onDeath.Invoke();
    }
}
