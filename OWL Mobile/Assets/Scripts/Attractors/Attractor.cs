using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    public static float G = 0.5f;
    Rigidbody rb;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();   
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        foreach (Attractee attractee in Attractee.attractees)
        {
            if (attractee == this)
                return;

            Attract(attractee.GetComponent<Rigidbody>());
        }
    }

    void Attract(Rigidbody rbToAttract)
    {
        if (rb.position == rbToAttract.position)
            return;

        Vector3 direction = rb.position - rbToAttract.position;
        float distance = direction.magnitude;
        float forceMagnitude = G * rb.mass * rbToAttract.mass / Mathf.Pow(distance, 2);
        Vector3 force = forceMagnitude * direction;
        rbToAttract.AddForce(force);
    }
}
