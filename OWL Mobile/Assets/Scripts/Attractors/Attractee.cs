using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractee : MonoBehaviour
{
    public static List<Attractee> attractees;

    private void OnEnable()
    {
        if (attractees == null)
            attractees = new List<Attractee>();

        attractees.Add(this);
    }
    private void OnDisable()
    {
        attractees.Remove(this);
    }
}
