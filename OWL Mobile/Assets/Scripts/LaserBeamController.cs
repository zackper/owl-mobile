using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class LaserBeamController : MonoBehaviour
{
    [SerializeField]
    private GameObject laserBeam;
    private Animator laserBeamAnimator;

    [SerializeField]
    private LineRenderer lr;
    [SerializeField]
    private VisualEffect vfxDrain;

    private Vector3[] positions = new Vector3[12];
    private Transform target = null;

    private void Awake()
    {
        laserBeamAnimator = laserBeam.GetComponent<Animator>();
        lr.useWorldSpace = true;

        Stop();
    }

    public void Play(Transform target)
    {
        laserBeamAnimator.SetBool("isOn", true);
        this.target = target;

        vfxDrain.gameObject.SetActive(true);
        vfxDrain.transform.localScale = target.localScale;
        //vfxDrain.Reinit();
    }
    public void Stop()
    {
        vfxDrain.gameObject.SetActive(false);
        laserBeamAnimator.SetBool("isOn", false);
    }

    private void Update()
    {
        if (target == null)
            return;

        if (Input.GetMouseButtonDown(0))
            laserBeamAnimator.SetTrigger("tap");

        // Set laser ends
        SetLrPositions(laserBeam.transform.position, target.position, 12);        
        lr.SetPositions(positions);

        // Set vfxDrain
        vfxDrain.transform.position = target.position;
    }

    private void SetLrPositions(Vector3 start, Vector3 end, int pointCount)
    {
        for(int i = 0; i < pointCount; i++)
        {
            positions[i] = Vector3.Lerp(start, end, (float)(i + 1) / (float)pointCount);
        }
    }
}
