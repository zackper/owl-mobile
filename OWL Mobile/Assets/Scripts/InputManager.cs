using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    private Dictionary<string, KeyCode> buttonMap = new Dictionary<string, KeyCode>();
    private Dictionary<string, UnityEvent> buttonEvents = new Dictionary<string, UnityEvent>();

    private void Update()
    {
        foreach (KeyValuePair<string, KeyCode> pair in buttonMap)
        {
            if (Input.GetKey(pair.Value))
                buttonEvents[pair.Key].Invoke();
        }
    }

    public void AddButton(string name, KeyCode code)
    {
        if (buttonMap.ContainsKey(name))
            buttonMap.Remove(name);
        buttonMap.Add(name, code);
        buttonEvents.Add(name, new UnityEvent());
    }
    public void Subscribe(string buttonName, UnityAction callback)
    {
        Debug.Assert(buttonEvents.ContainsKey(buttonName), "Button does not exist \'" + buttonName + "\'");
        buttonEvents[buttonName].AddListener(callback);
    }

    // Called in singleton awake
    private void InitButtonMap()
    {
        AddButton("w", KeyCode.W);
        AddButton("a", KeyCode.A);
        AddButton("s", KeyCode.S);
        AddButton("d", KeyCode.D);
        AddButton("q", KeyCode.Q);
        AddButton("e", KeyCode.E);
        AddButton("space", KeyCode.Space);
        AddButton("escape", KeyCode.Escape);

        AddButton("arrowUp", KeyCode.UpArrow);
        AddButton("arrowDown", KeyCode.DownArrow);
        AddButton("arrowLeft", KeyCode.LeftArrow);
        AddButton("arrowRight", KeyCode.RightArrow);
    }


    #region Singleton Pattern
    public static InputManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        InitButtonMap();
        DontDestroyOnLoad(this);
    }
    #endregion
}