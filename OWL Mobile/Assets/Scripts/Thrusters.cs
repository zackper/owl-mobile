using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Thrusters : MonoBehaviour
{
    private Rigidbody rb;

    private Vector3 currentTorque;
    
    [SerializeField]
    private float thrust = 50f;
    [SerializeField]
    private float maxSpeed = 50f;

    #region Rotation Smooth Damp
    private Quaternion smoothRotationVelocity = new Quaternion();
    [SerializeField]
    private float smoothRotationTime = 0.3f;
    #endregion
    #region Torque Smooth Damp
    private Vector3 lookAtDirection = Vector3.zero;
    private Vector3 smoothStopVelocity = Vector3.zero;
    [SerializeField]
    private float smoothStopTime = 1f;
    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        maxSpeed = SO_Spaceship.Instance.thrusterStrength;
        thrust = SO_Spaceship.Instance.thrusterStrength / 2;
    }
    private void Update()
    {
        SmoothLookAt();
    }

    public void AddThrust(Vector3 direction)
    {
        rb.AddForce(direction * thrust, ForceMode.Force);

        if(rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        }
    }
    public void SmoothLookAt()
    {
        // Get current rotation
        Quaternion currentEulerAngles = transform.rotation;
        // Get target rotation using lookat.
        transform.LookAt(transform.position + lookAtDirection);
        Quaternion targetEulerAngles = transform.rotation;
        // Smooth damp
        transform.rotation = QuaternionUtil.SmoothDamp(
        currentEulerAngles,
            targetEulerAngles,
            ref smoothRotationVelocity,
            smoothRotationTime
        );
    }
    public void SetLookAtDirection(Vector3 direction)
    {
        this.lookAtDirection = direction;
    }

    public void TryStabilize()
    {
        TryStabilizeThrust();
    }
    public void TryStabilizeThrust()
    {
        rb.velocity = Vector3.SmoothDamp(
            rb.velocity,
            Vector3.zero,
            ref smoothStopVelocity,
            smoothStopTime
        );
    }
}
