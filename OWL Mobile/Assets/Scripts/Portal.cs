using System.IO;
using System.Collections;
using UnityEngine;

public class Portal : MonoBehaviour
{
    private SO_Portal self;

    private void Awake()
    {
        self = Resources.Load<SO_Portal>(Path.Combine("SO", "Portals", "Portal"));
        Debug.Assert(self, "Wrong portal SO");
        self.ResetStability();
    }

    private IEnumerator NaturalDestabilization(float interval)
    {
        while (true)
        {
            yield return new WaitForSeconds(interval);
            self.stability -= self.tearDestabilizePerSecond;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ResourceMeteor ore = other.GetComponent<ResourceMeteor>();
        if (ore != null)
        {
            self.stability -= self.oreImpactDestabilize; // TODO: Change later to destabilize?

            // Destroy Meteor
            Destroy(other.gameObject);
            // Spawn Particles of impact
        }
    }
}
