using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipMovement : MonoBehaviour
{
    private Rigidbody rb;
    private Thrusters thrusters;

    [SerializeField]
    private PlayerInput input;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        thrusters = GetComponent<Thrusters>();

        //Subscribe to events
        QtEventManager.Instance.onEventStart += OnQTEventStart;
        QtEventManager.Instance.onEventEnd += OnQTEventEnd;
    }
    private void FixedUpdate()
    {
        if (input.joystick.Horizontal != 0 || input.joystick.Vertical != 0)
        {
            Move(new Vector3(input.joystick.Horizontal, 0f, input.joystick.Vertical));
        }
        else
            thrusters.TryStabilize();
    }
    public void Move(Vector3 direction)
    {
        rb.angularVelocity = Vector3.zero;

        // Thrust to direction
        thrusters.AddThrust(direction);
        // Rotate ship accordingly
        thrusters.SetLookAtDirection(direction);
    }

    // Register to QT_Events
    private void OnQTEventStart(QtEvent e)
    {
        thrusters.SetLookAtDirection(e.transform.position - transform.position);
    }
    private void OnQTEventEnd(QtEvent e)
    {

    }
}
