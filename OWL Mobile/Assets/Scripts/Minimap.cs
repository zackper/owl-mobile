using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour
{
    [SerializeField]
    private float showRadius = 400f;
    [SerializeField]
    private Color playerColor;
    [SerializeField]
    private Color resourceMeteorColor;
    [SerializeField]
    private Color spacetimeTearColor;
    [SerializeField]
    private Transform centerPivot;

    [SerializeField]
    private GameObject pinPrefab;

    public enum PinType
    {
        PLAYER,
        RESOURCE_METEOR,
        SPACETIME_TEAR
    }

    private class MapPin
    {
        public Transform obj;   // Refers to actual object in world space
        public Transform pin;      // Refers to pin in map

        public MapPin(Transform obj, Transform pin)
        {
            this.obj = obj;
            this.pin = pin;
        }
    }

    private List<MapPin> pins = new List<MapPin>();

    public void AddPin(Transform obj, PinType type)
    {
        GameObject pinObj = Instantiate(pinPrefab, centerPivot);
        pinObj.GetComponent<Image>().color = GetColor(type);

        MapPin pin = new MapPin(obj, pinObj.GetComponent<Transform>());
        pins.Add(pin);
    }
    public void RemovePin(Transform obj)
    {
        for(int i = 0; i < pins.Count; i++)
        {
            if (pins[i].obj == obj)
            {
                Destroy(pins[i].pin.gameObject); 
                pins.Remove(pins[i]);
                return;
            }
        }
    }
    private Color GetColor(PinType type)
    {
        if (type == PinType.PLAYER)
            return playerColor;
        else if (type == PinType.RESOURCE_METEOR)
            return resourceMeteorColor;
        else
            return spacetimeTearColor;
    }

    private void FixedUpdate()
    {
        float radius = GetComponent<RectTransform>().sizeDelta.x / 2;

        foreach (MapPin pin in pins)
        {
            pin.pin.localPosition = 
                new Vector3(pin.obj.position.x, pin.obj.position.z, 0f) * radius / showRadius;
        }
    }
}
