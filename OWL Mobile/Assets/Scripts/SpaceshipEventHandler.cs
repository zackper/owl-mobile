using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceshipEventHandler : MonoBehaviour
{
    private Rigidbody rb;
    private LaserBeamController laserBeamController;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        laserBeamController = GetComponent<LaserBeamController>();

        QtEventManager.Instance.onEventStart += OnQtEventStart;
        QtEventManager.Instance.onEventEnd += OnQtEventEnd;
    }

    private void OnQtEventStart(QtEvent e)
    {
        rb.isKinematic = true;
        laserBeamController.Play(e.transform);
    }
    private void OnQtEventEnd(QtEvent e)
    {
        rb.isKinematic = false;
        laserBeamController.Stop();
    }
}
