using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SpaceshipControls : MonoBehaviour
{
    private Rigidbody rb;
    private SpaceshipMovement spaceshipMovement;
        
    private void Start()
    {
        Application.targetFrameRate = 60;

        rb = GetComponent<Rigidbody>();
        spaceshipMovement = GetComponent<SpaceshipMovement>();
    }
}
