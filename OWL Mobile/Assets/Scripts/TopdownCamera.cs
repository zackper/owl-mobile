using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class TopdownCamera : MonoBehaviour
{
    private Camera self;

    [SerializeField]
    private Transform pivot;
    [SerializeField]
    private Transform target;

    [SerializeField]
    private float   cameraDistance; // How far away the camera is from the pivot
    [SerializeField]
    private Vector3 pivotEulerAngles = new Vector3(-25f, 0f, 0f);

    #region SmoothDamp Follow Variables
    [SerializeField]
    private float   followTime = 0.3f;
    private Vector3 followVelocity = Vector3.zero;
    #endregion
    #region SmoothDamp Distance Variables
    [SerializeField]
    private float   cameraDistanceTime = 0.3f;
    private Vector3 cameraDistanceVelocity = Vector3.zero;
    #endregion
    #region SmoothDamp Pivot Rotation Variables
    [SerializeField]
    private float pivotRotationSmoothTime = 0.3f;
    private Quaternion pivotRotationSmoothVelocity = new Quaternion();
    #endregion

    private void Start()
    {
        self = GetComponent<Camera>();
    }
    private void FixedUpdate()
    {
        SmoothFollowTarget();
        SmoothCameraDistance();
        SmoothPivotEulerAngles();
    }

    private void    SmoothFollowTarget()
    {
        // Smooth damp on X/Z position
        pivot.position = Vector3.SmoothDamp(
            pivot.position,
            target.position,
            ref followVelocity,
            followTime
        );
    }
    private void    SmoothCameraDistance()
    {
        // Smooth damp on Y position based on height and pivot.
        transform.localPosition = Vector3.SmoothDamp(
            transform.localPosition,
            Vector3.Normalize(transform.localPosition) * cameraDistance,
            ref cameraDistanceVelocity,
            cameraDistanceTime
        );
    }
    private void    SmoothPivotEulerAngles()
    {
        pivot.rotation = QuaternionUtil.SmoothDamp(
            pivot.rotation,
            Quaternion.Euler(pivotEulerAngles),
            ref pivotRotationSmoothVelocity,
            pivotRotationSmoothTime
        );
    }

    public void SetCameraDistance(float cameraDistance)
    {
        this.cameraDistance = cameraDistance;
    }
    public void SetPivotEulerAngles(Vector3 eulerAngles)
    {
        pivotEulerAngles = eulerAngles;   
    }
}
