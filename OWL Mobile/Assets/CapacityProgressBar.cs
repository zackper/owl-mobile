using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapacityProgressBar : ProgressBar
{
    private SO_PlayerResources.Resources resources;

    // Start is called before the first frame update
    void Start()
    {
        resources = SO_PlayerResources.Instance.levelResources;
        resources.onChange.AddListener(UpdateCapacity);
        UpdateCapacity();
    }
    private void OnDisable()
    {
        resources.onChange.RemoveListener(UpdateCapacity);
    }

    private void UpdateCapacity()
    {
        SetProgressFill(resources.capacity / SO_Spaceship.Instance.resourceCapacity);
    }
}
