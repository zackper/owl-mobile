﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedJoystick : Joystick
{
    protected new void Start()
    {
        base.Start();

        QtEventManager.Instance.onEventStart += OnQtEventStart;
        QtEventManager.Instance.onEventEnd += OnQtEventEnd;
    }

    public void OnQtEventStart(QtEvent e)
    {
        gameObject.SetActive(false);
    }
    public void OnQtEventEnd(QtEvent e)
    {
        gameObject.SetActive(true);
    }
}