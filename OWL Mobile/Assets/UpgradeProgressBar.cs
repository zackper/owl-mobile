using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeProgressBar : MonoBehaviour
{
    [SerializeField]
    private Color reachedUpgrade = Color.green;
    [SerializeField]
    private Color unreachedUpgrade = Color.black;
    [SerializeField]
    private GameObject progressBoxPrefab;

    private List<GameObject> children = new List<GameObject>();

    private void Awake()
    {
        Debug.Assert(progressBoxPrefab, "Prefab not initialized");
    }

    public void UpdateProgress(int current, int max)
    {
        //Clear all children
        foreach(var child in children)
            Destroy(child);
        children.Clear();

        // Spawn new
        for(int i = 0; i < max; i++)
        {
            GameObject child = Instantiate(progressBoxPrefab, this.transform);
            children.Add(child);

            if (i <= current)
                child.GetComponent<Image>().color = reachedUpgrade;
            else
                child.GetComponent<Image>().color = unreachedUpgrade;
        }
    }
}
