using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradePartPanel : MonoBehaviour
{
    [SerializeField]
    private SO_SpaceshipPart self;

    // UI References
    [SerializeField]
    private TMP_Text title;
    [SerializeField]
    private TMP_Text description;
    [SerializeField]
    private TMP_Text nextUpgradeLabel;
    [SerializeField]
    private TMP_Text ironCost;
    [SerializeField]
    private TMP_Text goldCost;
    [SerializeField]
    private TMP_Text platinumCost;
    [SerializeField]
    private TMP_Text upgradeCounter;

    [SerializeField]
    private Button upgradeButton;
    [SerializeField]
    private UpgradeProgressBar upgradeProgressBar;

    public void Start()
    {
        gameObject.SetActive(false);
        upgradeButton.onClick.AddListener(delegate
        {
            self.Upgrade();
            UpdateValues();
        });
    }
    private void Update()
    {
        if(self != null)
        {
            if(self.current == self.values.Count - 1)   // Upgrades are maxed.
            {
                nextUpgradeLabel.text = "MAX LEVEL";
                upgradeButton.gameObject.SetActive(false);
            }
            else                                        // There are available upgrades
            {
                nextUpgradeLabel.text = "NEXT UPGRADE COST:";
                upgradeButton.gameObject.SetActive(true);
                upgradeButton.interactable = self.CanUpgrade();
            }
        }
    }

    public void Init(SO_SpaceshipPart self)
    {
        this.self = self;
        UpdateValues();
    }

    private void UpdateValues()
    {
        title.text = self.title;
        description.text = self.description;
        upgradeCounter.text = (self.current + 1) + " / " + self.values.Count;

        if (self.current < self.values.Count - 1)
        {
            ironCost.text = self.costs[self.current + 1].iron.ToString("N1") + "T";
            goldCost.text = self.costs[self.current + 1].gold.ToString("N1") + "T";
            platinumCost.text = self.costs[self.current + 1].platinum.ToString("N1") + "T";
        }
        else
        {
            ironCost.text = "0T";
            goldCost.text = "0T";
            platinumCost.text = "0T";
        }

        upgradeProgressBar.UpdateProgress(self.current, self.values.Count);
    }
}
