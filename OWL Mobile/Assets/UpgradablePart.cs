using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradablePart : MonoBehaviour
{
    [SerializeField]
    private SO_SpaceshipPart self;

    private UpgradePartPanel upgradePanel;

    private void Awake()
    {
        upgradePanel = FindObjectOfType<UpgradePartPanel>();
    }

    private void OnMouseDown()
    {
        upgradePanel.gameObject.SetActive(true);
        upgradePanel.Init(self);
    }
}
