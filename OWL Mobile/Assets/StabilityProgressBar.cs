using System.IO;
using UnityEngine;
using System.Collections;

public class StabilityProgressBar : ProgressBar
{
    private SO_Portal portal;

    private void Start()
    {
        portal = Resources.Load<SO_Portal>(Path.Combine("SO", "Portals", "Portal"));
    }

    private void Update()
    {
        SetProgressFill(portal.stability / portal.maxStability);
    }
}
