using UnityEngine;

[CreateAssetMenu(fileName = "PlayerInput", menuName = "OWL/PlayerInput")]
public class PlayerInput : ScriptableObject
{
    public class Joystick
    {
        public float Horizontal = 0f;
        public float Vertical = 0f;
    }

    public Joystick joystick = new Joystick();
}
