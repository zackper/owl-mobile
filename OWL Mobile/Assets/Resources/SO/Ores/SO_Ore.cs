using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum OreType
{
    IRON, GOLD, PLATINUM
}

[CreateAssetMenu(fileName = "NewOre", menuName = "OWL/Ore")]
public class SO_Ore : ScriptableObject
{
    [field: SerializeField] public OreType  type { get; private set; }
    [field: SerializeField] public float    amount { get; private set; } = 0.13f;
    [field: SerializeField] public float    amountOffset { get; private set; } = 0.3f;
    [field: SerializeField] public int      repeats { get; private set; } = 10;

    [Space(10)]
    [Header("Visuals")]
    [field: SerializeField] public Color rockColor;
    [field: SerializeField] public Color metalColor;
    [field: SerializeField] public float scale = 1f;
}