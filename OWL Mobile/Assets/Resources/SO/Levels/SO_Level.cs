using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLevel", menuName = "OWL/Level")]
public class SO_Level : ScriptableObject
{
    [Header("Level Preview")]
    public string title;
    public int difficulty = 1;

    [Header("Collection of ore prefabs")]
    public List<GameObject>     orePrefabs;
    public GameObject           spacetimeTearPrefab;

    [Header("Ores")]
    [Header("List of ores and their respective amount")]
    public List<SO_Ore> ores;
    public List<int>    amount;
    [Header("Randomness")]
    public List<float>  additionalOreChance;
    [Header("Spacetime Tears")]
    public int          maxSpacetimeTears = 2;
    public float        spacetimeTearChancePerSecond = 0.1f;

    [Header("Distances")]
    public float maxDistance = 400f;
    public float safeDistance = 100f;
    [Header("Gravity Constant")]
    public float G = 0.5f;
}
