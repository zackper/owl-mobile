using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "NewSpaceshipPart", menuName = "OWL/SpaceshipPart")]
public class SO_SpaceshipPart : ScriptableObject
{

    public SO_Spaceship.PartType         type;

    [Header("Part basics")]
    public string       title;
    public string       description;
    [Header("Upgrade Levels")]
    public int                                  current = 0;
    public List<float>                          values;
    public List<SO_PlayerResources.Resources>   costs;

    public bool CanUpgrade()
    {
        if (current == values.Count - 1)
            return false;

        var resources = SO_PlayerResources.Instance.resources;
        if (
            costs[current + 1].iron <= resources.iron &&
            costs[current + 1].gold <= resources.gold &&
            costs[current + 1].platinum <= resources.platinum
        )
        {
            return true;
        }
        else
            return false;
    }
    public void Upgrade()
    {
        if (current < values.Count - 1)
        {
            ++current;
            SO_PlayerResources.Instance.RemoveResources(costs[current]);
            SO_Spaceship.Instance.UpdateValue(type, values[current]);
        }
        else
        {
            Debug.Assert(false, "All upgrades have been bought.");
            // Should not allow click.
        }
    }

#if UNITY_EDITOR
    #region Menu Items for faster operations
    [MenuItem("Assets/Reset Upgrade Levels")]
    static void ResetUpgradeLevels()
    {
        SO_SpaceshipPart[] parts = Resources.FindObjectsOfTypeAll<SO_SpaceshipPart>();
        foreach(var part in parts)
        {
            part.current = 0;
            SO_Spaceship.Instance.UpdateValue(part.type, part.values[part.current]);
        }
    }
    #endregion
#endif
}

public class SpaceshipPartEditorFunctions
{
    
}
