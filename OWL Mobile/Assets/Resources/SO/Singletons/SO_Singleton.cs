using UnityEngine;

public abstract class SO_Singleton<T> : ScriptableObject where T :ScriptableObject
{
    private static T _instance = null;

    public static T Instance
    {
        get
        {
            if(_instance == null)
            {
                T[] results = Resources.FindObjectsOfTypeAll<T>();
                if(results.Length == 0)
                {
                    Debug.LogError("SingletonScriptableObject: results length is zero");
                    return null;
                }
                if(results.Length > 1)
                {
                    Debug.LogError("SingletonScriptableObject: results length is greater than one.");
                    return null;
                }

                _instance = results[0];
                _instance.hideFlags = HideFlags.DontUnloadUnusedAsset;
            }
            return _instance;
        }
    }
}
