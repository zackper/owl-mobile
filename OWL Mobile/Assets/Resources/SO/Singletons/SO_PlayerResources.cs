using System;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewPlayerResources", menuName = "OWL/PlayerResourcesSingleton")]
public class SO_PlayerResources : SO_Singleton<SO_PlayerResources>
{
    [Serializable]
    public class Resources
    {
        public float iron = 0;
        public float gold = 0;
        public float platinum = 0;
        public float capacity
        {
            get
            {
                return iron + gold + platinum;
            }
        }

        public Resources(float iron, float gold, float platinum)
        {
            this.iron = iron;
            this.gold = gold;
            this.platinum = platinum;
        }
        public UnityEvent onChange = new UnityEvent();

        public static Resources operator +(Resources a, Resources b)
            => new Resources(a.iron + b.iron, a.gold + b.gold, a.platinum + b.platinum);
        public static Resources operator -(Resources a, Resources b)
            => new Resources(a.iron - b.iron, a.gold - b.gold, a.platinum - b.platinum);
    }

    [field: SerializeField] public Resources resources { get; private set; }
    [field: SerializeField] public Resources levelResources { get; private set; }

    public void RemoveResources(Resources toRemove)
    {
        resources = resources - toRemove;
        resources.onChange.Invoke();
    }

    public void CommitLevelResources()
    {
        resources.iron += levelResources.iron;
        resources.gold += levelResources.gold;
        resources.platinum += levelResources.platinum;
    }
    public void ResetLevelResources()
    {
        levelResources.iron = 0;
        levelResources.gold = 0;
        levelResources.platinum = 0;
    }
    public void AddLevelResources(OreType type, float amount)
    {
        if (levelResources.capacity + amount > SO_Spaceship.Instance.resourceCapacity)
            amount = SO_Spaceship.Instance.resourceCapacity - levelResources.capacity;

        switch (type)
        {
            case OreType.IRON:
                levelResources.iron += amount;
                break;
            case OreType.GOLD:
                levelResources.gold += amount;
                break;
            case OreType.PLATINUM:
                levelResources.platinum += amount;
                break;
            default:
                Debug.Assert(false, "Control flow should never reach here.");
                break;
        }

        levelResources.onChange.Invoke();
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void FirstInit()
    {
        Debug.Log("PlayerResources Singleton Init");
    }
}