using System;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSpaceshipSingleton", menuName = "OWL/SpaceshipSingleton")]
public class SO_Spaceship : SO_Singleton<SO_Spaceship>
{
    public float hardness = 100;
    public float thrusterStrength = 25f;
    public float resourceCapacity = 3; // In tons
    public float laserStrength = 1; // repeat count for QtEventspublic void Upgrade()

    public enum PartType
    {
        HARDNESS,
        THRUSTERS,
        CAPACITY,
        LASER_STRENGTH
    }
    public void UpdateValue(PartType type, float value)
    {
        switch (type)
        {
            case PartType.HARDNESS:
                hardness = value;
                break;
            case PartType.THRUSTERS:
                thrusterStrength = value;
                break;
            case PartType.CAPACITY:
                resourceCapacity = value;
                break;
            case PartType.LASER_STRENGTH:
                laserStrength = value;
                break;
            default:
                Debug.Assert(false, "Control flow should never reach this point");
                break;
        }
    }


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void FirstInit()
    {
        Debug.Log("Spaceship Singleton Init");
    }
}
