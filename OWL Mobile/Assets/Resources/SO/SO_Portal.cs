using UnityEngine;

[CreateAssetMenu(fileName = "NewPortal", menuName = "OWL/Portal")]
public class SO_Portal : ScriptableObject
{
    public float stability = -1;
    [field: SerializeField]
    public float maxStability { get; private set; } = 100f;

    [Header("Destabilization Amounts")]
    public float naturalDestablizePerSecond = 1f;
    public float oreImpactDestabilize = 10f;
    public float tearDestabilizePerSecond = 2f;
        
    public void ResetStability()
    {
        stability = maxStability;
    }
}
